import React from 'react';
import moment from 'moment';
import './DateTimePicker.css';
import 'material-datetime-picker/dist/material-datetime-picker.css';
import MaterialDateTimePicker from 'material-datetime-picker';

export default class DateTimePicker extends React.Component {

    // this component makes its own DOM updates, so tell React not to make any changes
    shouldComponentUpdate() {
        return false;
    }

    // use any values from `nextProps` to update the picker state, for example:
    componentWillReceiveProps(nextProps) {
        //this.picker.set(nextProps.newDate);
    }

    // the container element has been added to the DOM, so create the picker
    // shouldComponentUpdate returns false, so this is only called once
    componentDidMount() {
        // this.picker = new MaterialDateTimePicker({
        //     default: moment(),
        //     el: this.container
        // });
        this.picker = new MaterialDateTimePicker();
        this.picker.open();
        console.log("picker mounted")
    }

    // create the picker's wrapper component
    // shouldComponentUpdate returns false, so this is only called once
    render() {

        //viene mostrato pippo nella pagina
        //return <div>pippo</div>

        //non mostra nulla nella pagina :(
        return <div
          className="date-picker"
          ref={el => this.container = el}
        />;
    }
}
