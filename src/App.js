import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import HelloWord from './components/HelloWord';
import DateTimePicker from './components/DateTimePicker';

class App extends Component {
  render() {
    return (
      <div className="App">
        <HelloWord name="Sara" />
        <DateTimePicker ref="picker"/>
        <HelloWord name="Marco" />
        <div id="picker"></div>
      </div>
    );
  }
}

export default App;
