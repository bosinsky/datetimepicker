import React from 'react';
export default function HelloWord(props) {
    return <h1>Hello, {props.name}</h1>;
}